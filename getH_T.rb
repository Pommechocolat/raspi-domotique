#!/usr/bin/ruby
# coding : utf-8
# fichier: getH_T.rb
# auteur : jlm

def octets(rawRead)
  if rawRead[0]="1"
    rawRead = rawRead[1..-1]
  end
  bits = []
  bitsNb = 0
  bits[bitsNb] = ""
  sp = rawRead.split "0"
  sp.each { |str|
    taille = str.length
    case taille
    when 0
      #do nothing
    when 1, 2, 3
      bits[bitsNb] += "0"
    else
      bits[bitsNb] += "1"
    end
    if bits[bitsNb].length==8
      bitsNb+=1
      bits[bitsNb] = ""
    end
  }
  bits
end

def getTempHumid(bits)
  humidity = 0.0
  temp = 0.0
  if(bits.length>4)
    sommeCtrl = ((bits[0].to_i(2)+bits[1].to_i(2)+bits[2].to_i(2)+bits[3].to_i(2)) & 0x0FF).to_s(2)
    ctrl = bits[4].to_i(2).to_s(2)
    if sommeCtrl==ctrl
      humidity = (bits[0].to_i(2)*256+bits[1].to_i(2))/10.0
      temp = (bits[2].to_i(2)*256+bits[3].to_i(2))/10.0
      result = "ok"
    else
      result = "ko : #{sommeCtrl} != #{ctrl}"
    end
  else
    result = "ko : #{bits}" 
  end
  {"temp"=>temp, "humidity"=>humidity, "result"=>result}
end

#puts ENV
if ENV['_system_name']=="Debian" || ENV['PWD']=="/root"
  require_relative 'readH_T'

  DEVICE_DIR='/sys/class/gpio/'
  GPIO_NO='22'

  unless File.exist?(DEVICE_DIR+'gpio'+GPIO_NO)
    File.write(DEVICE_DIR+'export', GPIO_NO)
  end

  MyInlineC.new.write_to(DEVICE_DIR+'gpio'+GPIO_NO,1)
  sleep(3)
  MyInlineC.new.write_to(DEVICE_DIR+'gpio'+GPIO_NO,0)
  val = MyInlineC.new.read_nTime(DEVICE_DIR+'gpio'+GPIO_NO, 600)
  MyInlineC.new.write_to(DEVICE_DIR+'gpio'+GPIO_NO,0)
  r = getTempHumid(octets(val))
  time = Time.new.strftime("%Y-%m-%d %H:%M:%S")
  puts time+";"+r["temp"].to_s+";"+r["humidity"].to_s+";"+r["result"].to_s
end
