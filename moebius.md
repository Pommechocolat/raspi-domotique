# Moebius
C'est une distribution absolument minimal. La [version 2.0.1](http://sourceforge.net/projects/moebiuslinux/files/raspberry.stable/) est disponible au 3 juin 2015 sur sourceforge.net.
## Installation sur la SD
Après avoir décompréssé le fichier zip, on a une image minimal de 119Mo qui est à copier directement sur la SD avec la commande
```
dd if=moebius_2.0.1-r1.img of=/dev/sdb
```

## Premier boot
La [documentation](http://moebiuslinux.sourceforge.net/documentation/installation-guide/) (plutôt bien faite !) décrit au point 4 les premières oprérations réalisable pour configurer votre machine à votre main.

L'utilisateur est root avec le  mdp moebius

Le truc vraiment sympa, c'est que le ssh est configuré pour fonctionner avec root dès le départ. Pas besoin d'un clavier et d'une télé pour commancer à travailler. Un cable ethernet suffit !!! Enfin pour moi c'est le bonheur

## Test de la saisie de la température
La commande modprobe w1-gpio ssuffit pour avoir accès à la sonde de température avec la copmmande :
```
cat /sys/devices/w1_bus_master1/28-000005aaebad/w1_slave
```
C'est super rapide... mais y a pas de ruby et si l'installation de gcc a été possible, l'exécutable n'est pas là !
