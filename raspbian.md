# Raspbian
## récupération de l'OS
On peut trouver une version pour le Raspberry sur le site [raspbian.org](https://www.raspberrypi.org/downloads/raspbian/), en date du 05 mai 2015, à 1,04Go compressée.
Après avoir téléchargé l'archive qui convient (2015-05-05-raspbian-wheezy.zip), on décompresse l'archive. C'est une image qu'il faut installer sur la carte SD (ou sur une clé USB). Elle fait la bagatelle de 3,28Go ! Il faut donc une carte d'au moins 4Go. Je possède des clé USB de 8Go, c'est donc sur clé que je vais installer raspbian.

On utilisera la carte SD pour sa partition FAT afin de faire le boot initiale avant de le rediriger vers la clé USB.

## Installation de l'OS (sur clé USB)
La commande df -h (ou fdsik -l) permet de détermeiner ou se situe le device. Ce sera, l'emplacement /dev/sdc pour la clé USB.
Il faut pour procéder à l'installation démonter le device (ou plus exactement toutes ces partitions) :
```
umout /dev/sdc1
```
Une nouvelle commande df -h permet de constater que la carte SD n'est plus lisible. La copie de l'image se fait avec la commande dd : 
```
dd bs=4M if=2015-05-05-raspbian-wheezy.img of=/dev/sdc
sync
diff -s /dev/sdc 2015-05-05-raspbian-wheezy.img
``` 
Le processus de copie avec dd peut être assez long et aucune information ne vient confirmer la prograssion de l'écriture. Ensuite, la commande sync permet de s'assurer que toutes les écritures sur le device (SD ou USB) sont bien terminées. La commande diff permet de s'assurer que la copie est bien conforme à l'original.

Le diff nous dit que les fichiers sont différents !!! Pas cool.

Le résultat de la copie sur la clé donne 2 partitions
  - FAT32 en /dev/sdb1 (le c est changé à b car la SD n'est plus dans le lecteur) de taille 56Mo
  - Linux de 3Go en /dev/sdb2.
  
### Réglage du boot
Le processus d'installation sur la clé est terminée. Mais le Raspi démarre sur une partition FAT de la carte SD. Aussi il faut préparer cette dernière en réglant notamment le chemin vers la clé USB sur le Raspi. En bootant sur la carte Slitaz, il est possible d'installer la clé USB et de déterminer son adresse de device. Ce sera /dev/sda2.

Il est possible de faire un mount /dev/sda2 /mnt pour voir le contenu de la clé.

Il ne reste plus qu'a mettre sur une nouvelle carte dans une partition FAT le contenu du dossier boot en modifiant le fichier cmdline.txt pour mettre le chemin root=/dev/sda2.

## Premier boot en mode TV + clavier azerty
Le premier boot sur la TV ammène à l'écran de configuration de Raspbian sur le Raspi.
http://raspberrypis.net/reussir-le-premier-demarrage-de-son-raspberry-pi/
http://raspbian-france.fr/installer-raspbian-premier-demarrage-configuration/

Au premier boot, après le chargement très verbeux du système (on retrouve tout avec la commande dmesg), un écran de configuration est proposé avec 9 options :
  1 Expend Filesystème
  2 Change User Password -> root
  3 Enable Boot to Descktop/Scratch : par défaut console text
  4 Intenationalisation Option 
    1 change locale -> fr_FR.UTF-8 UTF-8 + en_EN.UTF-8 UTF-8
    2 change timezone -> Europe -> Paris
    3 change keybord layout -> French - French (Macintosh)
  5 Enable Camera
  6 Add to Rastrack
  7 Overclock
  8 Advanced Options
    A1
    ...
    A4 SSH Enable
    ...
    A0 Update
  9 About rapi-config

Ok, ça reboot et l'utilisateur pi peut se loguer avec le mot de passe défini.
Pour revenir à cet écran de configuration il faut entrer la commande 
```
sudo raspi-config
```

## Premier boot en SSH
Avec la commande 
```
ssh pi@192.168.0.9
```
on se connect sur le Raspi et tout va bien. Pour travailler avec le root il faut passer les commandes avec la commande sudo.

## Nettoyage de la distribution
dpkg --get-selections
sudo apt-get remove scratch
sudo apt-get remove xserver-common
sudo apt-get remove oracle-java8-jdk
sudo apt-get autoremove
