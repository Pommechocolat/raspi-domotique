## Slitaz
Personnellement je me suis intéressé à un OS Linux ultra léger : [slitaz](http://www.slitaz.org/fr/). Pour installer l'OS sur une carte SD, le plus simple est de partir d'un OS Linux. Sur Mac, ça semble bien galère. 

### récupération de l'OS
On peut trouver une version pour le Raspberry sur le site [slitaz pour ARM](http://arm.slitaz.org/rpi/), en date du 29 mars 2014, à 22Mo compressée sans environnemenrt graphique et 34Mo avec.
Après avoir téléchargé l'archive qui convient (slitaz-rpi-base-20140329.tar.tgz) sur le site Slitaz pour ARM, on décompresse l'archive. Elle est constitué de 4 éléments
  - boot : un dossier de 5,8Mo qui permet le boot de l'appareil. Il doit être installé sur une partition FAT
  - rootfs : c'est tout l'OS avec les outils et applications préinstallés : 52,5Mo tout mouillé. Il sera installé sur une grande partition au format EXT4
  - install.sh : un script qui s'occupe d'installé l'OS sur une carte SD
  - README : comme son nom l'indique

### Préparation de la carte SD
Il suffit d'insérer une carte de 256Mo en faisant au minimum deux partition : une FAT pour le secteur de boot et une Ext4 pour le linux. En plus on peut prévoir une partition pour le SWAP. On va préparer une carte SD avec trois partitions :
  - Boot en FAT
  - Swap
  - OS Slitaz en EXT4

Après avoir inséré la carte SD dans la machine, on se place en mode admin (root) et on utilise la commande
```
fdisk -l
```
pour connaitre son emplacement. Ce sera pour moi /dev/sdb. Il est possible de continuer avec fdisk en ligne de commmande...mais il est ausi possible d'utiliser l'outil graphique gparted (apt-get install gparted) pour le faire graphiquement.
La solution sera donc, pour une carte de 2Go :
  - /dev/sdb1 : fat16 : 16Mo
  - /dev/sdb2 : linux-swap : 512Mo
  - /dev/sdb3 : ext4 : 1,33Go

### Installation de l'OS
Après s'être installé dans le dossier de l'archive décompréssée, il suffit de lancer la commade ./install.sh.

La première (et dernière) question est de savoir ou installer l'OS. Se sera donc sur : sdb.
Quelques seconde plus tard, votre carte SD est prête à être installé dans votre raspi.

### Premier boot en mode TV + clavier azerty
Une fois la carte mise dans le raspi, la télé et le clavier branchés, le boot arrive sur la selection du clavier. On prendra fr-latin1 en azerty. Puis c'est le mot de passe root qui est demandé. Je choisi théoriquement le même soit root.
Un certain nombre de fichiers sont alors configurés et présenté à l'écran :
  - /etc/init.d/network
  - /etc/init.d/system.sh : hostname : slitaz, loopback, udhcpc, IP: 192.168.0.9, DNS: 212.27.40.240/241
  - /etc/init.d/local.sh : local: C, keymap: fr-latin, timeZone: UTC

Le résultat avec GIT est le suivant :

  - /etc/TZ : UTC
  - /etc/keymap.conf : fr-latin1
  - /etc/local.conf : LANG=C
  - /etc/resolv.conf : nameserver
  - /etc/shadow : reconfiguration de root
  - /etc/udev/rules.d/70-persistent-net.rules : nouveau fichier. Il contient l'adresse mac de eth0
  - /var/lib/slitaz/post-install : nouveau fichier avec une ligne vide 
  - /var/log/auth.log
  - /var/log/boot.log est créé avec les éléments affichés à l'écran
  - /var/log/daemon.log est créé
  - /var/log/dmesg.log est créé
  - /var/log/messages est créé
  - /var/log/wtmp binaire est modifié

### Autoriser ssh
Par défaut la connection ssh n'est pas en service. Pour lancer le service, il faut activer le démon nommé dropbear. Pour ce faire on édite le fichier /etc/rcS.conf et on ajoute dropbear à la liste de la variable RUN_DAEMONS.

Mais, dans la mesure ou aucun autre compte n'a été créer, je veux autoriser root à ce connecter. C'est pas bien mais c'est comme ça. Pour ça il faut modifier la configuration de dropbear dans le fichier /etc/daemons.conf en supprimant les configurations de sécurités que sont -w (pas de ssh pour root) -g (pas de login avec mot de passe pour root - ce qui signifie que root ne devrait éventuellement se connecter qu'à l'aide d'une clé ssh). Le mot de passe par défaut de root est root

### Premier boot en SSH
Avec la commande ssh root@192.168.0.9 on se connect sur le Raspi et tout va bien. Vu les config faites au départ par le processus de post-install, il est illusoire de mettre en route le SSH en premier !

#### Retrouver le Raspi sur le réseau
La commande "arp -a" permet de détecter les différents objets connecter sur le réseau en donnant leur adresse MAC. Cette du raspi est b8:27:eb:24:91:16.

#### Gestion des paquets
spk donne une information sur la gestion des paquets. spk-up permet de faire les upgrade nécessaires.
Le site http://cook.slitaz.org/cross/arm/packages/ liste tous les paques disponibles et notamment :
  - ruby-1.9.2-p180-arm.tazpkg

La commande :
```
spk-ls
```
liste les paquets installé avec une description sommaire et leur version actuel.

##### Suppression d'un fichier
```
spk-rm joe
```
supprime le package de l'éditeur joe.

##### récupération des paquets installable
Par défaut il manque le fichier /var/lib/tazpkg/packages.desc. Ce dernier peut-être mis à jour en lançant la commande spk-up. Mais attention, ne pas faire la mise à jour tout de suite. Il y a quelques gros soucis !

```
spk-up
n
```

##### Ajout de paquets
```
spk-add ruby
spk-rm tazirc
spk-add mercurial
spk-add tazdev
spk-add check
```
ajoute la version 1.9.2 de ruby.


spk-up devrait mettre à jour les paquets, mais il y a un problème dès la premiere mise à jour qui boucle indéfiniment.

spk-add tazpkg installe un système de version de paquets un peu plus évolué.

Dès lors pour installer ruby, la commande

tazpkg search ruby
fonctionne. Il est alors possible de trouver dans la liste fournie le paquets désiré. la commande

tazpkg -gi ruby-1.9.2-p180-arm
fait alors le nécessiare pour mettre à disposition un interpréteur ruby.

#### Mise à jour des paquets
Il y a des soucis expliqués dans ce post : http://forum.slitaz.org/topic/updates-to-slitaz-arm-via-spk-still-broken

vi /usr/lib/slitaz/libspk.sh
:139
i
Commenter les lignes 139 à 142 avec un # en début de ligne
esc
shift-ZZ
spk-up
y

Modifie TZ en supprimant UTC. J'annule
Modifie aussi les clés SSH de dropbear !! J'annule
Modifie rcS.conf. j'annule
Modifie etc/network.conf : j'annule


Suppression de etc/inittab : comprend pas trop pourquoi. J'annule

Les doosiers usr/bin, usr/lib, usr/share, usr/sbin sont laragement modifé avec des ajouts nombreux, comme les dossiers lib/ sbin/

Annulation des modification de etc/init.d
 
spk-rm slitaz-arm-configs
spk-add slitaz-arm-configs
réinstall le dropbear dans les daemons de /etc/rcS.conf

Au terme du processus de mise à jour, un certain nombre de fichiers sont supprimé, d'autre sont installé (notamment en ce qui concerne l'integace graphique dont je ne veux pas!). Avant de faire un reboot, il est important de vérifier que le système est toujours stable... ce qui n'est pas simple à faire !

Il y a notamment un souci avec slitaz-arm-configs qui détruit ou reconfigure les fichiers :
  - /etc/init.d/rcS
  - /etc/init.d/system.sh
  - /etc/rcS.conf
  - /etc/inittab
  
l'application slim nécessite 4 librairie qui sont absentes :
  - ligjpeg
  - libpng
  - xorg-libXft
  - xorg-libXmu
Or je ne veux pas d'interface graphique pour le moment. C'est un simple serveur que je veux manipuler !

#### Configurations date et heure
Pour avoir l'heure dès le démarrage de la machine, il est possible d'intérroger un serveur ntp. Encore faut-il que les services réseaux soit en place. On va donc à la fin du processus de boot, faire une lecture de date et heure. La fin du processus de boot est géré par le fichier /etc/init.d/local.sh :
```
echo "ntpd -dnqp fr.pool.ntp.org" >> /etc/init.d/local.sh
```

Par défaut l'heure retourné est UTC. En France, on va remplacer cette valeur du fichier /etc/TZ par la valeur adéquate :
```
echo "CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00" > /etc/TZ
```


### Recopie de slitaz vers la clé
La première opération consiste à formater la clé avec la commande mkfs.ext4 /dev/sda1.
La commande *'dd if=/dev/mmcblk0p3 of=/dev/sda1 bs=512'* permet copie bit a bit le contenu de la partition /root vers la clé qui sera donc reformaté par la même occasion.

### Serveur Web
Lancer le serveur web

startd httpd

Arrêt : stopd httpd

Une fois le serveur lancer on peut y accéder via son URL.

#### Prise d'humidité
Slitaz est un peu court pour faire le travail demandée : ruby + inline C. Aussi je vais tenter une installation d'une rasbian.


