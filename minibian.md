
Acte 2
Brancher le raspi sur une télé, avec clavier, écran, réseau, SD et alimentation, booter et entrer se logger : Mdp root : [raspberry](https://minibianpi.wordpress.com/features/). Attention le calvier est en mode Quewrty.

L'adresse Mac de mon Raspberry est b8:27:eb:24:91:16. Après un certain temps ! une Ip est distribuée par la Freebox : 192.168.0.77


Appareil photo :
300KP VGA 640x480 OV7670 6€14, 7€88, 9€40, 6€56
CMUcam5 Object Tracking Camera 86€
Cleo-Caméra Module 5 m Pixel ARDUINO-Cleo-CAM1 (FNL) : 46€, 55€
Uart TTL JPEG Couleur Caméra série Module 640x480 Pixels Pour Arduino DIY BA 17€

# Acte1 - Minibian
Obtenir [Minibian](https://minibianpi.wordpress.com) et l'installer sur une carte SD. Le fichier [tar.gz de la distrib est sur sourceforge.net](http://sourceforge.net/projects/minibian/files/?source=navbar). La version utilisée est 2016-03-12-jessie-minibian. On par d'un tar de 832,6 Mo qui contient une img de la même taille. L'image est installée (comme pour Moebius) avec la commande dd : dd if=fichier.img of=/dev/carteSD

~~~
sudo dd if=/Users/metzger9/Downloads/linux/2016-03-12-jessie-minibian.img of=/dev/disk2
~~

## Acte 2 - Premier boot
Brancher le raspi sur une télé, avec clavier, écran, réseau, SD et alimentation, booter et entrer se logger : Mdp root : [raspberry](https://minibianpi.wordpress.com/features/). Attention le calvier est en mode Quewrty.

Cette distrib met en place un demon ssh dès le départ. Le login/mdp est :
root / raspberry : https://minibianpi.wordpress.com/faq/

L'adresse Mac de mon Raspberry est b8:27:eb:24:91:16. Après un certain temps ! une Ip est distribuée par la Freebox : 192.168.0.xx
La connexion est aloors posible avec la comande :
~~~
ssh root@192.168.0.xx
~~~
### Installation d'une clé ssh
Pour une connexion sans mot de passe il faut installer la clé public de la machine de travail dans le fichier .ssh/authorized_keys du serveur minibian. Sur le minibian, via une connexion ssh, il faut construire le dossier .ssh (dans le dossier root) et lui donner les droits 700. 
~~~
mkdir .ssh
chmod 700 .ssh
~~

Puis il faut à partir d'un terminal sur le poste de travail il faut excuter la copie suivante :

~~~
scp .ssh/id_rsa.pub root@192.168.0.9:.ssh/authorized_keys
~~~

Avant il a fallu obtenir une pair de clé public/privée avec la commande ssh-keygen -t rsa -b 2048.

## Test température
Ça ne marche pas sur Raspbian, car il faut indiquer au noyau qu'il faut charger (ou prendre en charge) le gpio. Cela se fait en modifiant le fichier /boot/config.txt (il est dans la partition FAT de la SD) en lui ajoutant la ligne suivante (http://raspberrypi.stackexchange.com/questions/26623/ds18b20-not-listed-in-sys-bus-w1-devices) :
```
dtoverlay=w1-gpio
```
Un reboot et même plus besoin de modprobe. La sonde est visible directment à sont adresse habituelle :
```
cat /sys/devices/w1_bus_master1/28-000005aaebad/w1_slave
```

## Configuration

### Date et heure
La date peut être mise à la valeur française en modifiant le fichier /etc/localtime
```
cp -p /usr/share/zoneinfo/Europe/Paris /etc/localtime
```
J'ai aussi modifié (mais est-ce utile ?) le fichier /ect/timezone pour passer de Europe/London à Europe/Paris

### Configuration de locale
J'ai modifier /etc/default/locale pour passer de LANG=en_GB.UTF-8 à LANG=fr_FR.UTF-8.

### Packets installés
```
 dpkg --get-selections
```
permet d'obtenir la liste des pakages installés. Cette pour minimalRaspbian est :
adduser
apt
apt-utils
base-files
base-passwd
bash
bsdmainutils
bsdutils
ca-certificates
coreutils
cpio
cron
dash
debconf
debianutils
dialog
diffutils
dpkg
e2fslibs:armhf
e2fsprogs
fake-hwclock
findutils
gcc-4.7-base:armhf
gnupg
gpgv
grep
groff-base
gzip
hostname
ifupdown
initramfs-tools
initscripts
insserv
iproute
iputils-ping
isc-dhcp-client
isc-dhcp-common
keyboard-configuration
klibc-utils
kmod
less
libacl1:armhf
libapt-inst1.5:armhf
libapt-pkg4.12:armhf
libattr1:armhf
libblkid1:armhf
libbsd0:armhf
libbz2-1.0:armhf
libc-bin
libc6:armhf
libcap2:armhf
libcomerr2:armhf
libdb5.1:armhf
libedit2:armhf
libffi5:armhf
libgcc1:armhf
libgcrypt11:armhf
libgdbm3:armhf
libgnutls26:armhf
libgpg-error0:armhf
libgssapi-krb5-2:armhf
libidn11:armhf
libk5crypto3:armhf
libkeyutils1:armhf
libklibc
libkmod2:armhf
libkrb5-3:armhf
libkrb5support0:armhf
liblocale-gettext-perl
liblzma5:armhf
libmount1
libncurses5:armhf
libncursesw5:armhf
libopts25
libp11-kit0:armhf
libpam-modules:armhf
libpam-modules-bin
libpam-runtime
libpam0g:armhf
libpipeline1:armhf
libprocps0:armhf
libraspberrypi-bin
libraspberrypi0
libreadline6:armhf
libruby1.9.1
libselinux1:armhf
libsemanage-common
libsemanage1:armhf
libsepol1:armhf
libslang2:armhf
libss2:armhf
libssl1.0.0:armhf
libstdc++6:armhf
libtasn1-3:armhf
libtinfo5:armhf
libudev0:armhf
libusb-0.1-4:armhf
libustr-1.0-1:armhf
libuuid-perl
libuuid1:armhf
libwrap0:armhf
libyaml-0-2:armhf
linux-base
linux-image-3.18.0-trunk-rpi
locales
login
lsb-base
man-db
mawk
mount
multiarch-support
ncurses-base
ncurses-bin
net-tools
netbase
ntp
openssh-client
openssh-server
openssl
passwd
perl-base
procps
raspberrypi-bootloader
raspbian-archive-keyring
readline-common
rsyslog
ruby
ruby1.9.1
sed
sensible-utils
sysv-rc
sysvinit
sysvinit-utils
tar
tzdata
udev
util-linux
vim-common
vim-tiny
wget
zlib1g:armhf

Pour la mise à jour puis l'ajout de GCC et de Ruby se sera :
```
apt-get update
apt-get upgrade
apt-get distupgrade
apt-get install ruby
apt-get install gcc
apt-get clean
```

### Installation de RVM
La version ruby de Debian sur Raspi est la 1.9.1. C'est un peu court. On install donc RVM. Pour ce faire, il faut installer curl : apt-get install curl.
Ensuite il faut charger la clé du serveur rvm : gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
Puis enfin on charge RVM :  \curl -L https://get.rvm.io | bash -s stable --ruby


## Calling C/C++ from Ruby
Cette [page de amberbit.com](https://www.amberbit.com/blog/2014/6/12/calling-c-cpp-from-ruby/) est très intéressante et décrit quelques exemple d'écriture de routines C dans un programme ruby.

```
gem install inline
```

```
require 'inline
class InlineDHT
  inline do |builder|
  builder.include '<math.h>'
  builder.c '
    int fctC() {
      code C
    }'
  end
end

puts InlineDHT.new.fctC()
```
