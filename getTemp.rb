#!/usr/bin/ruby
# coding : utf-8
# fichier: getTemp.rb
# auteur : jlm

DEVICEDATAFILE = '/w1_slave'
DEVICE_DIR     = '/sys/bus/w1/devices/'

def getTemp(device_ref)
  temp = -62
  content = File.read(DEVICE_DIR+device_ref+DEVICEDATAFILE)
  lignes = content.split("\n")
  if lignes[0].end_with? "YES"
    temp = lignes[1][-5..-1]
  else
    puts "il y a un problème avec la sonde"
  end
  temp
end

def getDevices
  result = ""
  Dir.foreach(DEVICE_DIR) { |device|
     case device
     when /^28/
       temp = getTemp(device)
       if result==""
         result = temp
       else
         result = result+";"+temp
       end
       #puts temp
       break
     when "w1_bus_master1", ".", ".."
       #rien à faire
     else
       puts "Type of device not considerd : "+device
     end
  }
  result
end 

temps = getDevices
time = Time.new.strftime("%Y-%m-%d %H:%M:%S")
puts time+";"+temps