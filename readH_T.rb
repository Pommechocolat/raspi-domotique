#!/usr/bin/ruby
# coding : utf-8
# fichier: readH_T.rb
# auteur : jlm

require 'inline' #gem install inline

class MyInlineC
  inline do |builder|
    builder.include '<fcntl.h>'
    
    builder.c '
      static VALUE read_nTime(char *pinPath, int nTime) {
        VALUE res;        //valeur retournée à Ruby
        char path[100];   //chemin vers le ficher correspondant à la pin
        int hRead;        //handleur sur le fichier de lecture
        int i;            //cpt pour suivre le numbre de lecture
        char buf[1000];   //contenu des lectures
        
        //Préparation de la réponse
        res = rb_str_buf_new(0);
        //Mise en mode lecture
        sprintf(path, "%s/direction", pinPath);
        hRead = open(path, O_WRONLY);
        sprintf(buf, "in");
        write(hRead, buf, strlen(buf));
        close(hRead);
        //Lecture
        sprintf(path, "%s/value", pinPath);
        hRead = open(path, O_RDONLY);
        for(i=0; i<nTime; i++) {
          read(hRead, buf+i, 1);
          lseek(hRead, 0, SEEK_SET);
        }
        close(hRead);
        buf[i]=0; 
        rb_str_buf_cat(res, buf, strlen(buf));
        return res;
      } //end read_nTime
    ' #end builder read_nTime
    
    builder.c '
      static VALUE write_to(char *pinPath, int level) {
        VALUE res;        //valeur retournée à Ruby
        char path[100];   //chemin vers le ficher correspondant à la pin
        int hWrite;        //handleur sur le fichier d\'ecriture
        char buf[5];

        //Préparation de la réponse
        res = rb_str_buf_new(0);
        //Mise en mode écriture
        sprintf(path, "%s/direction", pinPath);
        hWrite = open(path, O_WRONLY);
        sprintf(buf, "out");
        write(hWrite, buf, strlen(buf));
        close(hWrite);
        //Ecriture
        sprintf(path, "%s/value", pinPath);
        hWrite = open(path, O_WRONLY);
        sprintf(buf, "%i", level);
        write(hWrite, buf, strlen(buf));
        close(hWrite);
        return res;
      }
    '
  end #inline
end #class MyInlineC

