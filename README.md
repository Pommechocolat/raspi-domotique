DOMOTIQUE
=========

Ce projet à pour but de mettre en œuvre différentes solution de domotique.

Je part dans un premier temps sur l'utilisation d'un Raspberry Pi (model B). 

J'étudie plusieurs système d'exploitation pouvant être installé et consommant un minimum de ressource tout en offrant les outils Ruby et C que je veux utiliser. Oui vous trouverais des code Ruby et non Python comme dans la plupart des projets sur Raspberry.

Ensuite je cherche à faire l'aquisition de données concernant la qualité de l'air. La première expérience (très simple) consiste à faire l'aquisition de température à l'aide d'une sonde [1-wire DS18B20](https://www.maximintegrated.com/en/products/analog/sensors-and-sensor-interface/DS18S20.html). On la trouve entre 2 et 5€ port compris en provenance de Chine 

Puis je présente la collecte de données de température et d'humidité à l'aide d'une [sonde DHT22](http://akizukidenshi.com/download/ds/aosong/AM2302.pdf). On la trouve au alentour de 4 à 9€ port compris en provenance de Chine.

Enfin j'ai branché une diode ou 2 relais (moins de 3$ port compris) pour envisager du contrôle commande.

# Présentation du Raspberry
Le [Raspberry Pi](http://www.raspberrypi.org/) est un petit ordinateur complet de la taille d'une carte bancaire (ou presque). En effet il possède, autour d'un processeur [ARM](https://fr.wikipedia.org/wiki/Architecture_ARM), Une sortie son en jack 3,5, une sortie vidéo composite RCA (pour les vieux téléviseurs), une sortie HDMI (pour les téléviseurs modernes), un slot pour accueillir une carte SD sur laquel sera installé l'OS, deux prises USB et une prise réseau RJ45 pour la vesion 512B. Cet ordinateur, alimenté en 5 Volts consomment entre 0,5 et 1 W.

Mais surtout cet ordinateur est équipé d'un connecteur GPIO pour gérer des entrés/sorties et autres modules extérieurs.

En version de base il est au prix de 35€ environ. Cependant il ne faudra pas oublier de lui adjiondre une alimentation 5V via une prise microUSB et une carte SD (minimum 256Mo mais si possible plus et surtout à haut débit).

# Choix du système d'exploitation
Le Raspeberry peut fonctionner avec plusieurs systèmes d'exploitation, y compris éventuellement Windows. Cependant c'est avec Linux que l'on a le moins à bourse délier et des solutions très viables.

Dans les différentes distributions Linux paquagées pour Rapsberry, il y en a deux qui retiennent l'attention :
  - [Raspbian](raspbian.md) construite sur une base Debian, référence dans le monde Linux
  - [Slitaz](slitaz.md) beaucoup moins connue et sans doute moins performante, et pour causse elle est ultra légère.

J'ai commencé avec Slitaz mais j'ai été assez vite arrété pour manque de compilateur C dans la distribution !
Raspbian est beaucoup plus lourd. Je n'ai pas été enchanté.

J'ai appris pas la suite l'existance de [Moebius](moebius.md), mais surtout celle qui a retenu depuis mon attention : [Minibian](minibian.md).

# booter sur une clé USB
J'ai une carte SD de 64Mo, trop petite pour un OS, et plein de clés USB ! De plus il parrait que les carte SD souffre lorqu'elles sont utilisées comme disque dur. Tout ça pour dire que je souhaite régler le boot pour qu'il y ai un miminum en lecture sur la carte SD et tout le reste en mode USB. Pour se faire je part de l'article [Pitié pour la carte SD de votre framboise314 : Bootez sur un disque dur USB](https://www.framboise314.fr/booter-le-raspberry-pi-sur-un-disque-dur-usb/) sur le site de la Framboise.

On va commencer à préparer une clé USB tout comme on a préparé la carte SD avec un système linux qui va bien. Bon d'accord la partie qui est en FAT on a a pas besoin... mais si on veut gagner de la place on vera plus tard.

Une fois notre clé prête avec sont système, il reste à modifier 2 fichiers. Le premier sur la partition de boot pour dire d'aller sur la clé plutôt que sur la carte SD pour trouver le rootfs. Puis il faut modifier sur la clé USB (donc sur le nouveau rootfs) le fichier /etc/fstab pour lui dire que le boot est toujours au même endroit (on ne change rien) mais qu'en revanche il faut faire les autres montages à partir de la clé USB et donc à nouveau changer /dev/mmcblk0p2 par /dev/sda2. On peut scripter c'est opération tel que ci-dessous :

~~~
sed -i 's/mmcblk0p2/sda2/' pathToSDCard/boot/cmdline.txt
sed -i 's/mmcblk0p2/sda2/' pathToUSBKey/etc/fstab
sed -i 's/vfat.defaults/vfat defaults,ro/' pathToUSBKey/etc/fstab  #pour mettre la SD en lecture seule.
~~~

# Exploitation du GPIO
http://www.raspberrypi-spy.co.uk/2012/06/simple-guide-to-the-rpi-gpio-header-and-pins/
## Sonde de Température DS1820
### Cablage
De gauche à droite en regrdant le plat de la sonde :
  - 1 Gnd -> pate 9 du gpio
  - 2 1-Wire -> pate 7 (gpio 4)
  - 3 VCC 3,3V -> pate 1 du GPIO
On cable une resistance de 4,7k ohms entre les pate 2 et 3 de la sonde. Une 10k ohms fonctionne aussi.

### Prise de température
Un tutoriel pour brancher une sonde [1-wire DS18B20](http://www.framboise314.fr/mesure-de-temperature-1-wire-ds18b20-avec-le-raspberry-pi/) est présenté sur le site très riche de [framboire314](http://www.framboise314.fr).
En branchant une sonde de température (Attention à ne pas se tromper dans le sens de branchement de la nape !! Il faut placer le PiCobbler à droite du Raspi avec le même sens d'écriture. La pate N°1 se trouve alors sous la nappe (en haut à gauche). On place la resistance entre la pate 1 (3,3V) et la pate 4 (lecture 1-wire). 
Il suffit alors de lancer la commade (en tant que root) :
```
modprobe w1-gpio
```
pour avoir accés au bus 1-wire sur le bus GPIO. On voit alors apparaitre le dossier w1_bus_master1 dans le dossier /sys/devices. On voit aussi apparaitre, un dossier w1_bus_master1 contenant lui même un dossier commançant par 28-xxx qui est le code des sondes de température et ou xxx est le numéro unique de la sonde de température. La lecture du fichier w1_master_slave_count dans le dossier w1_bus_master1 affiche 1 esclave si une seule sonde est branchée.

La commande suivante permet de reconnaitre les sondes de température : 
```
modprobe w1-therm
```
Alors, dans le dossier 28-xxx apparait le fichier w1_slave qui contient la température à lire :
```
cat /sys/devices/w1_bus_master1/28-000005aaebad/w1_slave 

51 01 4b 46 7f ff 0f 10 fe : crc=fe YES
51 01 4b 46 7f ff 0f 10 fe t=21062
```

Le programme de lecture de la température en ruby est [getTemp.rb](getTemp.rb).
La commande cron pour lire toutes les minutes la temérature est donné dans la crontab (crontab -l) :
* * * * * /root/getTemp.rb>>/root/temp.csv

## LED
### Cablage
Je m'appuis sur la [page](http://www.peatonet.com/fr/raspberry-pi-y-los-pines-gpio-controlando-un-led-con-bash-y-con-python/) pour piloter la LED.
Pour vérifier le fonctionement de la LED, on commance par la brancher en série avec une résistance de 470 ohms entre la pate 1 (VCC 3,3V) et la pate 9 (GND), le plat de la led coté GND.

Ensuite on va la laisser branchée sur le GND et l'on va metttre l'autre coté sur la pate 15 (GPIO22).

Pour travailler avec la pate 15 GPIO 22 on commance par déclarer comme fonctionnelle avec la commande 
```
echo 22 > /sys/class/gpio/export
```

A ce moment le chemin /sys/class/gpio/gpio22 existe et contient les fichiers suivants :
  - active_low
  - direction
  - edge
  - uevent
  - value
Plus 2 lien symbolique :
  - device
  - subsystem
  
L'opération suivante consiste à déclarer le sens des opération sur la pate 22 :
    - in pour la lecture
    - out pour l'écriture
    
Dans le cas de la LED, nous allons choisir l'écriture :
```
echo out > /sys/class/gpio/gpio22/direction
```

Puis il ne reste plus qu'a définir la valeur de la pate devenue sortie :
```
echo 1 > /sys/class/gpio/gpio22/value  #allumage de la LED
echo 0 > /sys/class/gpio/gpio22/value  #extinction de la LED
```

## Sonde d'humidité
### Cablage
On s'appuira sur l'[article](http://www.uugear.com/portfolio/read-dht1122-temperature-humidity-sensor-from-raspberry-pi/) pour lire la sonde DHT22.
Cette sonde possède 4 pate qui sont de gauche à droite :
  - 1 VCC 3,3V -> pate 1 du GPIO
  - 2 signal -> gpio22 pate 15
  - 3 non utilisé
  - 4 Gnd -> pate 9 du gpio
On cable une resistance de 10k ohms entre les pate 1 et 2 de la sonde

Pour lire la sonde, il faut d'abort lui indiquer que l'on veut la lire en mettant la pate de signal à 1 pendant 500 miliseconde (0,5 seconde) puis à 0 pendant 20 miliseconde (0,02s). Ensuite il faut se mettre en mode lecture du signal et récupérer les valeurs transmises par la sonde.

Ensuite on va lire le signal rendu par la sonde qui a la forme suivante en environ 5 miliseconde : 
  - 8 bits pour la valeur entière de l'humidité
  - 8 bits pour la valeur décimal de l'humidité
  - 8 bits pour la valeur entière de la température
  - 8 bits pour la valeur décimal de la température
  - 8 bits de checksum
5ms = 5000us
1bit 0 = 50us à 0 et 28us à 1 soit un total de 80us.
1bit 1 = 50us à 0 et 70us à 1 soit un total de 120us
https://www.sparkfun.com/datasheets/Sensors/Temperature/DHT22.pdf
En faisant 300 lectures sur les 5ms, on en déduit qu'une lecture sur 5000/312 = 16us se qui fait 5 lecure pour un bit à 0 et 7 à 8 lecture pour un bit à 1.
Il faut donc au moins 80 accès en lecture à la sonde pour être a peu prêt sûr de ne pas rater un bit en chemin. Seulement voila, en ruby on a au mieux 10 lectures en 4ms, en c sur le fichier /sys/class/gpio/gio22/value on atteint péniblement les 30 lecture (même avec un nice --20), c'est toujours 3 fois plus rapide... Mais il faut aller encore plus loin. La solution pourtant simple se trouve expliquée dans le [post stackOverflow](http://stackoverflow.com/questions/23650208/raspberry-pi-read-input-byte). Un simple lseek(fd, 0, SEEK_SET) fait le boulot.

On peut aussi essayer de lire directement dans la méloire le bit correspondant à la pin sur laquelle arrive le signal (15 pour le GPIO 22).

Je m'intéresse à l'[article](http://www.pieter-jan.com/node/15). http://stackoverflow.com/questions/23650208/raspberry-pi-read-input-byte

L'adresse de la variable gpio &gpio = x00b66e6e24 - ça change à chaque lancement du programme, mais toujours dans la même zone 00b66
La valeur de la variable gpio est : x0020200000
La valeur de la varialbe gpio.addr est : x0


Il pourrait être bien d'aller voir du coté de https://github.com/CAIMANICS/EzTempRH-for-Raspberry-Pi/tree/master/board pour trouver un montage de capteur plus pointu.

# Domotique #

## Liens utiles

* [Fibaro](http://www.fibaro.com/fr) : prises et autres appareils pilotés par un signal radio [Z-Wave](https://fr.wikipedia.org/wiki/Z-Wave) en 868,42 MHz

* [module E/R 433MHz](http://www.lightinthebox.com/fr/433m-module-emetteur-sans-fil-a-super-reaction-systeme-d-alarme-et-le-module-recepteur_p391009.html?currency=EUR&litb_from=paid_adwords_shopping) : un système à 1€ pour une communication radio. Utilisation pour [piloter des prises](http://blog.idleman.fr/raspberry-pi-12-allumer-des-prises-distance/)
* [Jeedom](https://www.jeedom.com/doc/documentation/installation/fr_FR/doc-installation.html) : un système de domotique libre qui peut être installé sur RasperryPi
* [Boutique domotique](http://www.domotique-store.fr/s/1/modules-zwave-plus)
* [Domoticz](http://www.domoticz.com)
https://kerberos.io
http://www.arducam.com/knowledge-base/arducam/
# Système de vidéo surveillance
Appareil photo :
300KP VGA 640x480 OV7670 6€14, 7€88, 9€40, 6€56
CMUcam5 Object Tracking Camera 86€
Cleo-Caméra Module 5 m Pixel ARDUINO-Cleo-CAM1 (FNL) : 46€, 55€
Uart TTL JPEG Couleur Caméra série Module 640x480 Pixels Pour Arduino DIY BA 17€
