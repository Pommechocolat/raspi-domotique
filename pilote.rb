#!/usr/bin/ruby
# coding : utf-8
# fichier: pilote.rb
# auteur : jlm

BOX='17'
CHAUFFAGE='27'
DEVICE_DIR='/sys/class/gpio/'

class EcritureGIPO
  def self.init(pin)
    unless File.exist?(DEVICE_DIR+'gpio'+pin)
      File.write(DEVICE_DIR+'export', pin)
      File.write(DEVICE_DIR+'gpio'+pin+'/direction', 'out')
    end
  end
  
  def self.alume(pin)
    self.init(pin)
    File.write(DEVICE_DIR+'gpio'+pin+'/value', '1')
  end

  def self.eteint(pin)
    self.init(pin)
    File.write(DEVICE_DIR+'gpio'+pin+'/value', '0')
  end
end

if(ARGV.size==1)
  case ARGV[0]
  when "alumeBox"
    EcritureGIPO.alume BOX
  when "eteintBox"
    EcritureGIPO.eteint BOX
  when "chauffage"
    EcritureGIPO.alume CHAUFFAGE
  when "arretChauffe"
    EcritureGIPO.eteint CHAUFFAGE
  else
    #envoi une alerte pour signaler un mauvaise commande par mail.
    puts "La commande ARGV #{ARGV} n'est pas reconnue"
  end
end
